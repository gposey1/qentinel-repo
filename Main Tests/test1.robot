# Tip: Everything that starts with # is a guidance for you and will not get executed.

*** Settings ***

Documentation           Starter suite for your personal website.
Library                 QWeb
Suite Setup             Open Browser    about:blank    ${BROWSER}
Suite Teardown          Close All Browsers

# Tip: The Settings section is where you define test suite level configuration.
#      Suite Setup and Teardown are actions that happen before and after a test suite run.
#      For first time users, we don't recommend to change them.

*** Variables ***

# TIP: You could also use "Firefox" (without quotes) below.
${BROWSER}            Chrome

*** Test Cases ***

# Tip: Test cases start from here. The structure of a test case is as follows:
#      Name of the test
#          [Documentation]    A short explanation of the test.
#          Test steps
#
#      A new test case starts from the beginning of the line. Each test step starts with four
#      spaces, followed by a PaceWord, at least four spaces, and its arguments.

SFDC login
    [Documentation]     First test case
    GoTo                https://login.salesforce.com/
    VerifyText          Use Custom Domain
    TypeText            Username    grahamposey@copado.com
    TypeText            Password    073084July!
    #ClickElement         //*[@id\="Login"]
# Pro Tip: Make sure that when each test case starts, your application is in a well-defined
#          initial state. The test cases above use GoTo with a fixed URL to set the initial
#          application state, but we recommend starting each test case with the dedicated
#          AppState PaceWord. The test case below starts with AppState Home, where Home is an
#          initial state defined in the Keywords section at the end of the file. More initial
#          states could be defined as needed.
